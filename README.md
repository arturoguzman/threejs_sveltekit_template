# ThreeJs - Sveltekit - Vite3 template

A nice spinning cube template for a fast ThreeJs and Sveltekit project kickstart :)

Vite 3, Dat.GUI, GSAP, Tailwind, Luxon and Lodash ready.

## Creating a project & Developing

```bash
git clone git@gitlab.com:arturoguzman/threejs_sveltekit_template.git
```

```bash
npm install
```

```bash
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
