export const randomColour = (opacity) => {
	return `rgb( ${Math.floor(Math.random() * 255)} ${Math.floor(Math.random() * 255)} ${Math.floor(
		Math.random() * 255
	)} / ${opacity})`;
};

export const randomColourNoOpacity = () => {
	return `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(
		Math.random() * 255
	)})`;
};

export const colours = {
	a: Math.floor(Math.random() * 255),
	b: Math.floor(Math.random() * 255),
	c: Math.floor(Math.random() * 255),
	d: Math.floor(Math.random() * 255),
	e: Math.floor(Math.random() * 255),
	f: Math.floor(Math.random() * 255),
	g: Math.floor(Math.random() * 255),
	h: Math.floor(Math.random() * 255),
	i: Math.floor(Math.random() * 255)
};

export const testingChange = () => {
	setInterval(() => {
		colours.a++;
		colours.b++;
		colours.c++;
		colours.d++;
		colours.e++;
		colours.f++;
		colours.g++;
		colours.h++;
		colours.i++;
		console.dir(colours);
	}, 500);
};
