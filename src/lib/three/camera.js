//@ts-nocheck
import * as THREE from 'three';
import { Core } from './core';

export class Camera {
	constructor() {
		this.core = new Core();
		this.sizes = this.core.sizes;
		this.scene = this.core.scene;
		this.canvas = this.core.canvas;
		this.createPerspectiveCamera();
		this.createOrthographicCamera();
	}
	createPerspectiveCamera() {
		this.perspectiveCamera = new THREE.PerspectiveCamera(35, this.sizes?.aspectRatio, 0.1, 1000);
		this.scene?.add(this.perspectiveCamera);
		this.perspectiveCamera.position.x = -100;
		this.perspectiveCamera.position.y = 50;
		this.perspectiveCamera.position.z = 90;
	}
	createOrthographicCamera() {
		this.frustrum = 5;
		this.orthographicCamera = new THREE.OrthographicCamera(
			this.sizes?.width / -2,
			this.sizes?.width / 2,
			this.sizes?.height / 2,
			this.sizes?.height / -2,
			-1000,
			1000
		);
		this.scene?.add(this.orthographicCamera);
		this.helper = new THREE.CameraHelper(this.perspectiveCamera);
		this.scene.add(this.helper);
	}
	start() {}
	resize() {
		this.perspectiveCamera.aspect = this.sizes?.aspectRatio;
		this.perspectiveCamera?.updateProjectionMatrix();

		this.orthographicCamera.left = this.sizes?.width / -2;
		this.orthographicCamera.right = this.sizes?.width / 2;
		this.orthographicCamera.top = this.sizes?.height / 2;
		this.orthographicCamera.bottom = this.sizes?.height / -2;
		this.orthographicCamera?.updateProjectionMatrix();
	}
	update() {}
}
