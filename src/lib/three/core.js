// @ts-nocheck

import * as THREE from 'three';
/* import * as dat from 'dat.gui'; */
import { Camera } from './camera';
import { Lights } from './lights';
import { Renderer } from './renderer';
import { Sizes } from './utils/sizes';
import { World } from './universe/world';
import { Controls } from './utils/controls';
import { Timeline } from './timeline/main';
import { Loaders } from './utils/loaders';
import { Helpers } from './utils/helpers';

export class Core {
	static base;
	constructor(_canvas, _width, _height, _gui) {
		if (Core.base) {
			return Core.base;
		}
		Core.base = this;
		this.gui = _gui || '';
		this.canvas = _canvas;
		this.scene = new THREE.Scene();
		this.sizes = new Sizes({ _width, _height });
		this.lights = new Lights();
		this.camera = new Camera();
		this.controls = new Controls();
		this.renderer = new Renderer();
		this.loaders = new Loaders();
		this.world = new World();
		this.timeline = new Timeline();
		this.helpers = new Helpers();
		this.animate();
	}
	animate() {
		requestAnimationFrame(() => this.animate());
		this.camera.update();
		this.renderer.update();
		this.controls.update();
	}
	resize() {
		this.camera.resize();
		this.renderer.resize();
	}
}
