//@ts-nocheck
import * as THREE from 'three';
import { Core } from './core';

export class Lights {
	constructor() {
		this.core = new Core();
		this.sizes = this.core.sizes;
		this.scene = this.core.scene;
		this.canvas = this.core.canvas;
		const ambientLight = new THREE.AmbientLight(0xffffff, 0.1);
		const spotLight = new THREE.SpotLight(0xa30000, 1, 100, 30, 0, 2);
		spotLight.position.set(-20, 50, 0);
		/* const spotLightHelper = new THREE.SpotLightHelper(spotLight); */
		this.scene.add(ambientLight);
		this.scene.add(spotLight);
		/* 		this.scene.add(spotLightHelper); */
	}
	start() {}
	resize() {}
	update() {}
}
