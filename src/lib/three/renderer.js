import * as THREE from 'three';
import { Core } from './core';

export class Renderer {
	constructor() {
		this.core = new Core();
		this.sizes = this.core.sizes;
		this.scene = this.core.scene;
		this.canvas = this.core.canvas;
		this.camera = this.core.camera;
		this.setRenderer();
	}
	setRenderer() {
		this.renderer = new THREE.WebGLRenderer({
			canvas: this.canvas,
			antialias: true
		});
		/* this.renderer.physicallyCorrectLights = true; */
		this.renderer.outputEncoding = THREE.sRGBEncoding;
		this.renderer.toneMapping = THREE.CineonToneMapping;
		this.renderer.toneMappingExposure = 1.75;
		this.renderer.shadowMap.enabled = true;
		this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
		this.renderer.setSize(this.sizes?.width, this.sizes?.height);
		this.renderer.setPixelRatio(this.sizes?.pixelRatio);
	}
	start() {
		this.renderer?.render(this.scene, this.camera?.perspectiveCamera);
	}
	resize() {
		this.renderer?.setSize(this.sizes?.width, this.sizes?.height);
		this.renderer?.setPixelRatio(this.sizes?.pixelRatio);
	}
	update() {
		this.renderer?.render(this.scene, this.camera?.perspectiveCamera);
	}
}
