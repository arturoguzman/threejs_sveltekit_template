/* import _ from 'lodash'; */
import { gsap } from 'gsap';
import { Core } from '../core';

export class Timeline {
	constructor() {
		this.core = new Core();
		this.world = this.core.world;
		gsap.fromTo(
			this.world.cube.cube.rotation,
			{
				y: -Math.PI
			},
			{
				y: Math.PI,
				yoyo: true,
				repeat: -1,
				duration: 4,
				ease: 'power4.inOut'
			}
		);
	}
}
