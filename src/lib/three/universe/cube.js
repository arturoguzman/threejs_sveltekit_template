// @ts-nocheck
import _ from 'lodash';
import * as THREE from 'three';
import { Core } from '../core';
import { randomColourNoOpacity } from '../../scripts/randomColour';

export class Cube {
	constructor() {
		this.core = new Core();
		this.gui = this.core.gui;
		const geometry = new THREE.BoxBufferGeometry(10, 10, 10);
		const material = new THREE.MeshPhongMaterial({
			color: randomColourNoOpacity(),
			specular: 0x050505,
			shininess: 80
		});
		this.cube = new THREE.Mesh(geometry, material);
		this.cube.rotation.set(
			_.random(-3.1416, 3.1416, true),
			_.random(-3.1416, 3.1416, true),
			_.random(-3.1416, 3.1416, true)
		);
		this.gui.add(this.cube.position, 'x', -100, 100);
		this.gui.add(this.cube.position, 'y', -100, 100);
		this.gui.add(this.cube.position, 'z', -100, 100);
		this.gui.add(this.cube.rotation, 'x', -3.1416, 3.1416);
		this.gui.add(this.cube.rotation, 'y', -3.1416, 3.1416);
		this.gui.add(this.cube.rotation, 'z', -3.1416, 3.1416);
		this.gui.add(material.color, 'r', 0, 1, 0.00001);
		this.gui.add(material.color, 'g', 0, 1, 0.00001);
		this.gui.add(material.color, 'b', 0, 1, 0.00001);
	}
}
