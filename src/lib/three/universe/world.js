// @ts-nocheck
import { Core } from '../core';
import { Cube } from './cube';

export class World {
	constructor() {
		this.core = new Core();
		this.scene = this.core.scene;
		this.cube = new Cube();
		this.scene.add(this.cube.cube);
	}
}
