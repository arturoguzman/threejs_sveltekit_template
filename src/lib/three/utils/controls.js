import { Core } from '../core';
import { ArcballControls } from 'three/examples/jsm/controls/ArcballControls.js';

export class Controls {
	constructor() {
		this.core = new Core();
		this.scene = this.core.scene;
		this.camera = this.core.camera;
		this.canvas = this.core.canvas;
		this.arcballControl = new ArcballControls(this.camera?.perspectiveCamera, this.canvas);
	}
	update() {
		this.arcballControl.update();
	}
}
