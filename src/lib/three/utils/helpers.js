import * as THREE from 'three';
import { Core } from '../core';

export class Helpers {
	constructor() {
		this.core = new Core();
		this.sizes = this.core.sizes;
		this.scene = this.core.scene;
		this.canvas = this.core.canvas;
		this.camera = this.core.camera;
		this.renderer = this.core.renderer;
		this.setHelpers();
	}
	setHelpers() {
		const size = 100;
		const divisions = 10;
		const gridHelper = new THREE.GridHelper(size, divisions);
		this.scene.add(gridHelper);
	}
	start() {}
	resize() {}
	update() {}
}
