//@ts-nocheck
import * as THREE from 'three';
import { Core } from '../core';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader';
import { gsap } from 'gsap';

export class Loaders {
	constructor() {
		this.core = new Core();
		this.sizes = this.core.sizes;
		this.scene = this.core.scene;
		this.canvas = this.core.canvas;
		this.svgLoader = new SVGLoader();
		/* svgLoader.load('/bear.svg', (data) => {
			this.load(data);
		}); */
	}
	load(data) {
		let idArray = [];
		let groupArray = new Map();
		const xml = data.xml;
		const paths = data.paths;
		for (const item of xml.childNodes) {
			if (item.id) {
				idArray = [...idArray, item];
				groupArray.set(item.id, new THREE.Group());
			}
		}
		const group = new THREE.Group();
		for (let i = 0; i < paths.length; i++) {
			let name = '';
			const path = paths[i];
			for (const item of idArray) {
				if (item.contains(path.userData.node)) {
					name = item.id;
				}
			}
			const material = new THREE.MeshBasicMaterial({
				color: path.color,
				side: THREE.DoubleSide,
				depthWrite: false
			});
			const shapes = SVGLoader.createShapes(path);
			for (let j = 0; j < shapes.length; j++) {
				const shape = shapes[j];
				const geometry = new THREE.ExtrudeBufferGeometry(shape, { depth: 20, bevelEnabled: false });
				const mesh = new THREE.Mesh(geometry, material);
				mesh.name = name;
				groupArray.get(name).add(mesh);
			}
		}
		for (const item of idArray) {
			groupArray.get(item.id).name = item.id;
			group.add(groupArray.get(item.id));
		}
		group.scale.set(0.2, -0.2, 0.2);
		const box = new THREE.Box3().setFromObject(group);
		const size = new THREE.Vector3();
		box.getSize(size);

		const yOffset = size.y / -2;
		const xOffset = size.x / -2;

		// Offset all of group's elements, to center them
		group.children.forEach((item) => {
			item.position.x = xOffset;
			item.position.y = yOffset;
		});
		gsap.to(group.rotation, {
			z: 1,
			duration: 2,
			yoyo: true,
			repeat: -1
		});
		this.scene.add(group);
	}
	start() {}
	resize() {}
	update() {}
}
