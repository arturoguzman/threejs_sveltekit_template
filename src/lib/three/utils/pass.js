import * as THREE from 'three';
import { Core } from '../core';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { SavePass } from 'three/examples/jsm/postprocessing/SavePass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { BlendShader } from 'three/examples/jsm/shaders/BlendShader';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import { gsap } from 'gsap';

export class Renderer {
	constructor() {
		this.core = new Core();
		this.sizes = this.core.sizes;
		this.scene = this.core.scene;
		this.canvas = this.core.canvas;
		this.camera = this.core.camera;
		this.renderer = this.core.renderer;
		this.setPass();
	}
	setPass() {
		this.composer = new EffectComposer(this.renderer);
		// render pass
		const renderPass = new RenderPass(this.scene, this.camera?.orthographicCamera);
		const renderTargetParameters = {
			minFilter: THREE.LinearFilter,
			magFilter: THREE.LinearFilter,
			stencilBuffer: true
		};

		// save pass
		const savePass = new SavePass(
			new THREE.WebGLRenderTarget(this.sizes?.width, this.sizes?.height, renderTargetParameters)
		);

		// blend pass
		const blendPass = new ShaderPass(BlendShader, 'tDiffuse1');
		blendPass.uniforms['tDiffuse2'].value = savePass.renderTarget.texture;
		/* blendPass.uniforms['mixRatio'].value = 0.8; */
		gsap.fromTo(
			blendPass.uniforms['mixRatio'],
			{
				value: 0.8
			},
			{
				value: 0,
				repeat: -1,
				duration: 1,
				ease: 'slow.inOut'
			}
		);
		// output pass
		const outputPass = new ShaderPass(CopyShader);
		outputPass.renderToScreen = true;

		// adding passes to composer
		this.composer.addPass(renderPass);
		this.composer.addPass(blendPass);
		this.composer.addPass(savePass);
		this.composer.addPass(outputPass);
	}
	start() {
		/* this.composer?.render(this.scene, this.camera?.orthographicCamera); */
	}
	resize() {
		/* 		this.renderer?.setSize(this.sizes?.width, this.sizes?.height);
		this.renderer?.setPixelRatio(this.sizes?.pixelRatio); */
	}
	update() {
		/* this.renderer?.render(this.scene, this.camera?.orthographicCamera); */
		this.composer?.render();
	}
}
