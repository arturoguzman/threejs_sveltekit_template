//@ts-nocheck

export class Sizes {
	constructor({ _width, _height, _pixelRatio }) {
		this.width = _width;
		this.height = _height;
		this.aspectRatio = this.width / this.height;
		this.pixelRatio = _pixelRatio || 2;
	}
	resize({ _width, _height, _aspectRatio, _pixelRatio }) {
		this.width = _width;
		this.height = _height;
		this.aspectRatio = _aspectRatio;
		this.pixelRatio = _pixelRatio;
	}
}
